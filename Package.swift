// swift-tools-version:5.0
import PackageDescription

let package = Package(
    name: "Audio",
    products: [
        .library(name: "Audio", type: .static, targets: ["Audio"])
    ],
    targets: [
        .target(
            name: "Audio",
            path: "Audio"
        )
    ]
)

