//
//  Sound.swift
//  Audio
//
//  Created by Endoral on 12.07.2018.
//  Copyright © 2018 Endoral. All rights reserved.
//

import Foundation

/// Sound management in the application.
public final class Sound {
    /// Player to play the specified sound.
    private let player: AudioPlayer
    
    /// The container for the player from the name of the sound in the specified bundle.
    /// - Parameters:
    ///     - name: Sound name in the specified bundle.
    ///     - bundle: Bundle in which to search for the requested sound.
    ///     - id: Player id.
    ///     - callback: Callback function that returns the player that has finished playing.
    private init?(name: String, bundle: Bundle, id: AudioPlayer.ID, callback: AudioPlayer.Callback?) {
        for ext in AudioPlayerManager.extensions {
            guard let url = bundle.url(forResource: name, withExtension: "") ?? bundle.url(forResource: name, withExtension: ext) else {
                continue
            }
            
            do {
                let data = try Data(contentsOf: url)
                let player = try AudioPlayer(data: data, id: id, callback: callback)
                
                Sound.prepare(player: player)
                
                self.player = player
            } catch {
                Debug.print("\(Sound.self): Couldn't play sound with name \(name).\(ext)\n - Reason: \(error.localizedDescription)\n - Error: \(error)")
                
                return nil
            }
            
            return
        }
        
        Debug.print("\(Sound.self): Couldn't find \(name) file\n - Available file formats: \(AudioPlayerManager.extensions.joined(separator: " "))")
        
        return nil
    }
    
    /// The container for the player from the specified URL.
    /// - Parameters:
    ///     - url: A URL identifying the sound file to play. The audio data must be in a format supported by Core Audio.
    ///     - id: Player id.
    ///     - callback: Callback function that returns the player that has finished playing.
    private init?(url: URL, id: AudioPlayer.ID, callback: AudioPlayer.Callback?) {
        guard FileManager.default.fileExists(atPath: url.path) else {
            Debug.print("\(Sound.self): Couldn't find file at \(url)")
            
            return nil
        }
        
        do {
            let data = try Data(contentsOf: url)
            let player = try AudioPlayer(data: data, id: id, callback: callback)
            
            Sound.prepare(player: player)
            
            self.player = player
        } catch {
            Debug.print("\(Sound.self): Couldn't play sound from \(url)\n -Reason: \(error.localizedDescription)\n - Error: \(error)")
            
            return nil
        }
    }
    
    /// The container for the player from the specified data.
    /// - Parameters:
    ///     - data: The data object containing the audio.
    ///     - id: Player id.
    ///     - callback: Callback function that returns the player that has finished playing.
    private init?(data: Data, id: AudioPlayer.ID, callback: AudioPlayer.Callback?) {
        do {
            let player = try AudioPlayer(data: data, id: id, callback: callback)
            
            Sound.prepare(player: player)
            
            self.player = player
        } catch {
            Debug.print("\(Sound.self): Couldn't play sound from data\n - Reason: \(error.localizedDescription)\n - Error: \(error)")
            
            return nil
        }
    }
}

extension Sound {
    /// Current active players.
    public static var players: Array<AudioPlayer> {
        return Array(audioPlayerManager.players.values)
    }
    
    /// The value above which the volume of the players cannot be set (except for cases when the player volume is set directly). 1 by default.
    public static var maxVolume: Float {
        return audioPlayerManager.maxVolume
    }
    
    /// Last non-zero manager volume. No adjustment for mute state. Serves to restore the player volume after mute. Value of maxVolume (1) by default.
    public static var currentVolume: Float {
        return audioPlayerManager.currentVolume
    }
    
    /// Mute state. If true, then the volume value is set to 0, otherwise the volume value of the players is set to the currentVolume value. False by default.
    public static var isMuted: Bool {
        return audioPlayerManager.isMuted
    }
    
    /// Current player volume. Adjusted for mute state. If muted then 0, otherwise currentVolume.
    public static var volume: Float {
        return audioPlayerManager.volume
    }
    
    /// The delegate of the manager of audio players to track global audio events and events related to the playback status of players.
    public static var delegate: AudioPlayerManagerDelegate? {
        get {
            return audioPlayerManager.delegate
        }
        
        set {
            audioPlayerManager.delegate = newValue
        }
    }
    
    /// Manager for audio players.
    private static let audioPlayerManager = AudioPlayerManager(
        currentVolumeKey: "Audio.Sound.currentVolume",
        maxVolumeKey: "Audio.Sound.maxVolume",
        isMutedKey: "Audio.Sound.isMuted"
    )
}

extension Sound {
    /// Create a unique identifier for the player.
    /// - Returns: Player id.
    public class func makePlayerID() -> AudioPlayer.ID {
        return "Audio.Sound.Player.id:\((Date().timeIntervalSince1970 * 1000.0).rounded())"
    }
    
    /// Player for specified id.
    /// - Parameters:
    ///     - id: Player id.
    /// - Returns: The player associated with the specified id or nil if the player associated with the specified id is missing.
    public class func player(for id: AudioPlayer.ID) -> AudioPlayer? {
        return audioPlayerManager.player(for: id)
    }
    
    /// Play sound from application bundle by specified name.
    /// - Parameters:
    ///     - name: Sound name in the specified bundle.
    ///     - bundle: Bundle in which to search for the requested sound. Bundle.main by default.
    ///     - id: Player id. Return value of makePlayerID() func by default.
    ///     - volumePercent: Player volume in percent. 100 by default.
    ///     - completion: Callback function that returns the player that has finished playing. Nil by default.
    /// - Returns: A player associated with a sound by the specified name or nil in case of failure.
    @discardableResult
    public class func play(name: String, bundle: Bundle = .main, id: AudioPlayer.ID = makePlayerID(), volumePercent: Float = 100, completion: (AudioPlayer.Callback)? = nil) -> AudioPlayer? {
        guard let player = Sound(name: name, bundle: bundle, id: id, callback: completion)?.player else {
            return nil
        }
        
        audioPlayerManager.add(player: player, volumePercent: volumePercent)
        
        player.play()
        
        return player
    }
    
    /// Play sound for specified URL.
    /// - Parameters:
    ///     - url: A URL identifying the sound file to play. The audio data must be in a format supported by Core Audio.
    ///     - id: Player id. Return value of makePlayerID() func by default.
    ///     - volumePercent: Player volume in percent. 100 by default.
    ///     - completion: Callback function that returns the player that has finished playing. Nil by default.
    /// - Returns: A player associated with a sound by the specified url or nil in case of failure.
    @discardableResult
    public class func play(url: URL, id: AudioPlayer.ID = makePlayerID(), volumePercent: Float = 100, completion: (AudioPlayer.Callback)? = nil) -> AudioPlayer? {
        guard let player = Sound(url: url, id: id, callback: completion)?.player else {
            return nil
        }
        
        audioPlayerManager.add(player: player, volumePercent: volumePercent)
        
        player.play()
        
        Debug.print("\(Sound.self): \(url) \(player.id) \(Sound.isMuted || !player.isPlaying ? "-" : "+")")
        
        return player
    }
    
    /// Play the specified from data.
    /// - Parameters:
    ///     - data: The data object containing the audio.
    ///     - id: Player id. Return value of makePlayerID() func by default.
    ///     - volumePercent: Player volume in percent. 100 by default.
    ///     - completion: Callback function that returns the player that has finished playing. Nil by default.
    /// - Returns: A player from associated with a sound from the data object or nil in case of failure.
    @discardableResult
    public class func play(data: Data, id: AudioPlayer.ID = makePlayerID(), volumePercent: Float = 100, completion: (AudioPlayer.Callback)? = nil) -> AudioPlayer? {
        guard let player = Sound(data: data, id: id, callback: completion)?.player else {
            return nil
        }
        
        audioPlayerManager.add(player: player, volumePercent: volumePercent)
        
        player.play()
        
        Debug.print("\(Sound.self): player from data with \(player.id) \(Sound.isMuted || !player.isPlaying ? "-" : "+")")
        
        return player
    }
    
    /// Stop playback of all sounds.
    public class func stopAll() {
        audioPlayerManager.stopAll()
    }
    
    /// Set mute state for all players.
    /// - Parameters:
    ///     - isMuted: Mute state. If true then sets the player volume to 0, otherwise sets the player volume to currentVolume.
    ///     - fadeDuration: Volume fade duration.
    public class func set(isMuted state: Bool, fadeDuration: TimeInterval = 0.0) {
        audioPlayerManager.set(isMuted: state, fadeDuration: fadeDuration)
    }
    
    /// Set the volume for all players.
    /// - Parameters:
    ///     - volume: Sound volume value from 0 to maxVolume.
    ///     - fadeDuration: Volume fade duration.
    /// - Returns: True if the specified value is set successfully, otherwise false.
    @discardableResult
    public class func set(volume value: Float, fadeDuration: TimeInterval = 0.0) -> Bool {
        return audioPlayerManager.set(volume: value, fadeDuration: fadeDuration)
    }
    
    /// Set the maximum volume value for all players.
    /// - Parameters:
    ///     - maxVolume: Maximum volume value from >0 to 1.
    /// - Returns: True if the specified value is set successfully, otherwise false.
    @discardableResult
    public class func set(maxVolume value: Float) -> Bool {
        return audioPlayerManager.set(maxVolume: value)
    }
    
    /// Set the volume at the first launch.
    /// - Parameters:
    ///     - defaultVolume: Volume value from 0 to maxVolume.
    /// - Returns: True if the specified value is set successfully, otherwise false.
    @discardableResult
    public class func set(defaultVolume value: Float) -> Bool {
        return audioPlayerManager.set(defaultVolume: value)
    }
    
    /// Reset the flag responsible for locking the ability to set the default volume value.
    public class func resetDefaultVolumeState() {
        audioPlayerManager.resetDefaultVolumeState()
    }
}

extension Sound {
    /// Preparing the player for playback.
    /// - Parameters:
    ///     - player: Audio player for playing sound.
    private class func prepare(player: AudioPlayer) {
        player.delegate = audioPlayerManager
        
        player.prepareToPlay()
    }
}

