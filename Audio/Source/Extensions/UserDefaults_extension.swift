//
//  UserDefaults_extension.swift
//  Audio
//
//  Created by Endoral on 21.02.17.
//  Copyright © 2017 Endoral. All rights reserved.
//

import Foundation

/// An interface to the user’s defaults database, where you store key-value pairs persistently across launches of your app.
typealias Prefs = UserDefaults

extension UserDefaults {
    /// Waits for any pending asynchronous updates to the defaults database and returns; this method is unnecessary and shouldn't be used.
    /// - Returns: True if the data was saved successfully to disk, otherwise false.
    @discardableResult
    class func synchronize() -> Bool {
        return standard.synchronize()
    }
    
    /// Removes values for all keys from the current user‘s defaults database.
    class func removeAll() {
        standard.dictionaryRepresentation().keys.forEach { key in
            standard.removeObject(forKey: key)
        }
        
        synchronize()
    }
    
    /// Removes the value of the specified key from the current user‘s defaults database.
    /// - Parameters:
    ///     - key: The key whose value you want to remove.
    class func removeValue(for key: String) {
        standard.removeObject(forKey: key)
    }
    
    /// Checks if the object associated with the specified key exists in the current user‘s defaults database.
    /// - Parameters:
    ///     - key: A key in the current user‘s defaults database.
    /// - Returns: True if the object associated with the specified key exists, otherwise false.
    class func isExist(valueFor key: String) -> Bool {
        return standard.object(forKey: key) != nil
    }
    
    /// Adds a value for the specified key in the current user‘s defaults database.
    /// - Parameters:
    ///     - value: The value added to the current user‘s defaults database.
    ///     - key: The key to put the value to the current user‘s defaults database.
    class func setValue<T>(_ value: T, for key: String) {
        standard.set(value, forKey: key)
    }
    
    /// Get the value for the specified key from the current user‘s defaults database.
    /// - Parameters:
    ///     - key: The key associated with the requested value.
    ///     - defaultValue: The default value if the object with the specified key is nil or the specified key is missing.
    /// - Returns: The value associated with the specified key, or the default value if the object with the specified key is nil or the specified key is missing.
    class func getValue<T>(for key: String, default defaultValue: T) -> T {
        return standard.object(forKey: key) as? T ?? defaultValue
    }
}

