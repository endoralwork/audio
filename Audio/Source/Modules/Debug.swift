//
//  Debug.swift
//  Audio
//
//  Created by John Doe on 14.09.2020.
//  Copyright © 2020 Endoral. All rights reserved.
//

// #todo: add? and fix? tests ???

import Foundation

/// Debug settings.
public struct Debug {
    /// Responsible for outputting debugging information to the console. Default value is `false`.
    public static var isLogging: Bool = false
    
    static func print(_ items: Any..., separator: String = " ", terminator: String = "\n", isLogging: Bool = isLogging) {
        guard isLogging else {
            return
        }
        
        Swift.print(items, separator: separator, terminator: terminator)
    }
}

