//
//  BackgroundMusic.swift
//  Audio
//
//  Created by Endoral on 12.07.2018.
//  Copyright © 2018 Endoral. All rights reserved.
//

import Foundation

/// Background music management in the application.
public final class BackgroundMusic {
    /// Player to play the specified background music.
    private let player: AudioPlayer
    
    /// The container for the player from the name of the background music in the specified bundle.
    /// - Parameters:
    ///     - name: Background music name in the specified bundle.
    ///     - bundle: Bundle in which to search for the requested background music.
    ///     - id: Player id.
    ///     - callback: Callback function that returns the player that has finished playing.
    private init?(name: String, bundle: Bundle, id: AudioPlayer.ID, callback: AudioPlayer.Callback?) {
        for ext in AudioPlayerManager.extensions {
            guard let url = bundle.url(forResource: name, withExtension: "") ?? bundle.url(forResource: name, withExtension: ext) else {
                continue
            }
            
            do {
                let data = try Data(contentsOf: url)
                let player = try AudioPlayer(data: data, id: id, callback: callback)
                
                BackgroundMusic.prepare(player: player)
                
                self.player = player
            } catch {
                Debug.print("\(BackgroundMusic.self): Couldn't play sound with name \(name).\(ext)\n - Reason: \(error.localizedDescription)\n - Error: \(error)")
                
                return nil
            }
            
            return
        }
        
        Debug.print("\(BackgroundMusic.self): Couldn't find \(name) file\n - Available file formats: \(AudioPlayerManager.extensions.joined(separator: " "))")
        
        return nil
    }
    
    /// The container for the player from the name of the background music in the specified bundle.
    /// - Parameters:
    ///     - url: A URL identifying the background music file to play. The audio data must be in a format supported by Core Audio.
    ///     - id: Player id.
    ///     - callback: Callback function that returns the player that has finished playing.
    private init?(url: URL, id: AudioPlayer.ID, callback: AudioPlayer.Callback?) {
        guard FileManager.default.fileExists(atPath: url.path) else {
            Debug.print("\(BackgroundMusic.self): Couldn't find file at \(url)")
            
            return nil
        }

        do {
            let data = try Data(contentsOf: url)
            let player = try AudioPlayer(data: data, id: id, callback: callback)
            
            BackgroundMusic.prepare(player: player)
            
            self.player = player
        } catch {
            Debug.print("\(BackgroundMusic.self): Couldn't play sound from \(url)\n - Reason: \(error.localizedDescription)\n - Error: \(error)")
            
            return nil
        }
    }
    
    /// The container for the player from the name of the background music in the specified bundle.
    /// - Parameters:
    ///     - data: The data object containing the audio.
    ///     - id: Player id.
    ///     - callback: Callback function that returns the player that has finished playing.
    private init?(data: Data, id: AudioPlayer.ID, callback: AudioPlayer.Callback?) {
        do {
            let player = try AudioPlayer(data: data, id: id, callback: callback)
            
            BackgroundMusic.prepare(player: player)
            
            self.player = player
        } catch {
            Debug.print("\(BackgroundMusic.self): Couldn't play music from data \(data)\n - Reason: \(error.localizedDescription)\n - Error: \(error)")
            
            return nil
        }
    }
}

extension BackgroundMusic {
    /// The value above which the volume of the players cannot be set (except for cases when the player volume is set directly). 1 by default.
    public static var maxVolume: Float {
        return audioPlayerManager.maxVolume
    }
    
    /// Last non-zero manager volume. No adjustment for mute state. Serves to restore the player volume after mute. Value of maxVolume (1) by default.
    public static var currentVolume: Float {
        return audioPlayerManager.currentVolume
    }
    
    /// Mute state. If true, then the volume value is set to 0, otherwise the volume value of the players is set to the currentVolume value. False by default.
    public static var isMuted: Bool {
        return audioPlayerManager.isMuted
    }
    
    /// Current player volume. Adjusted for mute state. If muted then 0, otherwise currentVolume.
    public static var volume: Float {
        return audioPlayerManager.volume
    }
    
    /// The delegate of the manager of audio players to track global audio events and events related to the playback status of players.
    public static var delegate: AudioPlayerManagerDelegate? {
        get {
            return audioPlayerManager.delegate
        }
        
        set {
            audioPlayerManager.delegate = newValue
        }
    }
    
    /// The current state of the module. Ttrue if the player is successfully loaded and ready to play, otherwise false.
    public static var isReady: Bool {
        return audioPlayerManager.player(for: playerID) != nil
    }
    
    /// A Boolean value that indicates whether the audio player is playing.
    public static var isPlaying: Bool {
        return audioPlayerManager.player(for: playerID)?.isPlaying ?? false
    }
    
    /// The playback point, in seconds, within the timeline of the sound associated with the audio player.
    public static var currentTime: Float {
        return Float(audioPlayerManager.player(for: playerID)?.currentTime ?? 0)
    }
    
    /// Player to play background music.
    public static var player: AudioPlayer? {
        return audioPlayerManager.player(for: playerID)
    }
    
    /// Manager for audio players.
    private static let audioPlayerManager = AudioPlayerManager(
        currentVolumeKey: "Audio.BackgroundMusic.currentVolume",
        maxVolumeKey: "Audio.BackgroundMusic.maxVolume",
        isMutedKey: "Audio.BackgroundMusic.isMuted"
    )
    
    /// Player ID for background music.
    private static let playerID = AudioPlayer.ID("\(BackgroundMusic.self)")
}

extension BackgroundMusic {
    /// Start playing background music by the specified name from the specified bundle.
    /// - Parameters:
    ///     - name: The name of the file with the background music in the specified bundle. "main" by default.
    ///     - bundle: Bundle to search for a file with background music by the specified file name. Bundle.main by default.
    ///     - volumePercent: Percentage of sound volume of the added player from 0 to 100, where 100 is equal to maxVolume.
    /// - Returns: Player to play background music or nil in case of failure.
    @discardableResult
    public class func play(name: String = "main", bundle: Bundle = .main, volumePercent: Float = 100) -> AudioPlayer? {
        guard let player = audioPlayerManager.player(for: playerID) ?? BackgroundMusic(name: name, bundle: bundle, id: playerID, callback: nil)?.player else {
            return nil
        }
        
        if audioPlayerManager.players[player.id] == nil {
            audioPlayerManager.add(player: player, volumePercent: volumePercent)
        } else {
            player.setVolumePercent(volumePercent, maxVolume: volume)
        }
        
        player.play()
        
        return player
    }
    
    /// Start playing background music stored on the specified URL.
    /// - Parameters:
    ///     - url: Local URL to the file with background music.
    ///     - volumePercent: Percentage of sound volume of the added player from 0 to 100, where 100 is equal to maxVolume.
    /// - Returns: Player to play background music or nil in case of failure.
    @discardableResult
    public class func play(url: URL, volumePercent: Float = 100) -> AudioPlayer? {
        guard let player = audioPlayerManager.player(for: playerID) ?? BackgroundMusic(url: url, id: playerID, callback: nil)?.player else {
            return nil
        }
        
        if audioPlayerManager.players[player.id] == nil {
            audioPlayerManager.add(player: player, volumePercent: volumePercent)
        } else {
            player.setVolumePercent(volumePercent, maxVolume: volume)
        }
        
        player.play()
        
        return player
    }
    
    /// Start playing background music from the specified data.
    /// - Parameters:
    ///     - data: Data containing the media file of one of the extensions available for this module.
    ///     - volumePercent: Percentage of sound volume of the added player from 0 to 100, where 100 is equal to maxVolume.
    /// - Returns: Player to play background music or nil in case of failure.
    @discardableResult
    public class func play(data: Data, volumePercent: Float = 100) -> AudioPlayer? {
        guard let player = audioPlayerManager.player(for: playerID) ?? BackgroundMusic(data: data, id: playerID, callback: nil)?.player else {
            return nil
        }
        
        if audioPlayerManager.players[player.id] == nil {
            audioPlayerManager.add(player: player, volumePercent: volumePercent)
        } else {
            player.setVolumePercent(volumePercent, maxVolume: volume)
        }
        
        player.play()
        
        return player
    }
    
    /// Pauses playback; sound remains ready to resume playback from where it left off.
    public class func pause() {
        audioPlayerManager.player(for: playerID)?.pause()
    }
    
    /// Stops playback and undoes the setup needed for playback.
    public class func stop() {
        audioPlayerManager.player(for: playerID)?.stop()
    }
    
    /// Stop the player and unload it from memory.
    @discardableResult
    public class func release() -> AudioPlayer? {
        return audioPlayerManager.removePlayer(for: playerID)
    }
    
    /// Set mute state for the player.
    /// - Parameters:
    ///     - isMuted: Mute state. If true then sets the player volume to 0, otherwise sets the player volume to currentVolume.
    ///     - fadeDuration: Volume fade duration.
    public class func set(isMuted state: Bool, fadeDuration: TimeInterval = 0.0) {
        audioPlayerManager.set(isMuted: state, fadeDuration: fadeDuration)
    }
    
    /// Set the volume for the player.
    /// - Parameters:
    ///     - volume: Sound volume value from 0 to maxVolume.
    ///     - fadeDuration: Volume fade duration.
    /// - Returns: True if the specified value is set successfully, otherwise false.
    @discardableResult
    public class func set(volume value: Float, fadeDuration: TimeInterval = 0.0) -> Bool {
        return audioPlayerManager.set(volume: value, fadeDuration: fadeDuration)
    }
    
    /// Set the maximum volume value for the player.
    /// - Parameters:
    ///     - maxVolume: Maximum volume value from >0 to 1.
    /// - Returns: True if the specified value is set successfully, otherwise false.
    @discardableResult
    public class func set(maxVolume value: Float) -> Bool {
        return audioPlayerManager.set(maxVolume: value)
    }
    
    /// Set the volume at the first launch.
    /// - Parameters:
    ///     - defaultVolume: Volume value from 0 to maxVolume.
    /// - Returns: True if the specified value is set successfully, otherwise false.
    @discardableResult
    public class func set(defaultVolume value: Float) -> Bool {
        return audioPlayerManager.set(defaultVolume: value)
    }
    
    /// Reset the flag responsible for locking the ability to set the default volume value.
    public class func resetDefaultVolumeState() {
        audioPlayerManager.resetDefaultVolumeState()
    }
}

extension BackgroundMusic {
    /// Preparing the player for playback.
    /// - Parameters:
    ///     - player: Audio player for playing background music.
    private class func prepare(player: AudioPlayer) {
        player.delegate = audioPlayerManager
        player.numberOfLoops = -1
        
        player.prepareToPlay()
    }
}

