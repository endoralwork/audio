//
//  AudioPlayer.swift
//  Audio
//
//  Created by Endoral on 12.07.2018.
//  Copyright © 2018 Endoral. All rights reserved.
//

import AVFoundation

/// AVAudioPlayer wrapper.
public final class AudioPlayer: AVAudioPlayer {
    /// Callback function that returns the player that has finished playing.
    /// - Parameters:
    ///     - player: Player ending playback.
    public typealias Callback = (_ player: AudioPlayer) -> Void
    
    /// Typealias for player id type.
    public typealias ID = AnyHashable
    
    /// Player ID to track its status in the player manager.
    public let id: ID
    
    /// Playback time tracking status.
    public var isPlaybackTracking: Bool {
        return timer != nil
    }
    
    /// Callback after playback ends.
    private let callback: Callback?
    
    /// Timer for periodically tracking playback time.
    private var timer: Timer?
    
    /// Implemented by subclasses to initialize a new object (the receiver) immediately after memory for it has been allocated.
    /// - Parameters:
    ///     - id: Player ID to track its status in the player manager.
    ///     - callback: Callback after playback ends.
    required init(id: ID, callback: Callback?) {
        self.id = id
        self.callback = callback
        
        super.init()
    }
    
    /// Initializes and returns an audio player using the specified URL and file type hint.
    /// - Parameters:
    ///     - url: A URL identifying the sound file to play. The audio data must be in a format supported by Core Audio.
    ///     - id: Player ID to track its status in the player manager.
    ///     - fileTypeHint: A UTI that is used as a file type hint. The supported UTIs are defined in File Format UTIs. Nil by default.
    ///     - callback: Callback after playback ends.
    /// - Throws: NSError object describes the error.
    required init(contentsOf url: URL, id: ID, fileTypeHint utiString: String? = nil, callback: Callback?) throws {
        self.id = id
        self.callback = callback
        
        try super.init(contentsOf: url, fileTypeHint: utiString)
    }
    
    /// Initializes and returns an audio player using the specified data and file type hint.
    /// - Parameters:
    ///     - data: The data object containing the audio.
    ///     - id: Player ID to track its status in the player manager.
    ///     - fileTypeHint: A UTI that is used as a file type hint. The supported UTIs are defined in File Format UTIs. Nil by default.
    ///     - callback: Callback after playback ends.
    /// - Throws: NSError object describes the error.
    required init(data: Data, id: ID, fileTypeHint utiString: String? = nil, callback: Callback?) throws {
        self.id = id
        self.callback = callback
        
        try super.init(data: data, fileTypeHint: utiString)
    }
    
    deinit {
        timer?.invalidate()
        timer = nil
    }
    
    /// Stops playback and undoes the setup needed for playback.
    public override func stop() {
        super.stop()

        if let audioPlayerDelegate = delegate as? AudioPlayerDelegate {
            audioPlayerDelegate.audioPlayerDidStop?(self)
        }
    }
}

extension AudioPlayer {
    /// Set new volume as a percentage.
    /// - Parameters:
    ///     - value: Percentage of sound volume of the added player from 0 to maxVolume.
    ///     - maxVolume: The maximum available volume for the player. Range is (0.0 to 1.0].
    ///     - fadeDuration: Volume fade duration.
    public func setVolumePercent(_ value: Float, maxVolume: Float = 1.0, fadeDuration: TimeInterval = 0.0) {
        let newVolume: Float
        
        newVolume = value == 100
            ? maxVolume
            : round(1000000 * ((maxVolume / 100) * value)) / 1000000
        
        if #available(iOS 10.0, *) {
            if fadeDuration > 0.0 {
                setVolume(newVolume, fadeDuration: fadeDuration)
            } else {
                volume = newVolume
            }
        } else {
            volume = newVolume
        }
    }
    
    /// Track playback time.
    /// - Parameters:
    ///     - interval: Tracking interval.
    ///     - completion: Block with the playback time as an argument.
    /// - Returns: False if tracking is already in progress, true otherwise.
    @available(iOS 10.0, *)
    @discardableResult
    public func addTimeTracking(with interval: TimeInterval, completion: @escaping (TimeInterval) -> Void) -> Bool {
        guard timer == nil else {
            return false
        }
        
        timer = Timer.scheduledTimer(
            withTimeInterval: interval,
            repeats: true,
            block: { [weak self] _ in
                guard let strongSelf = self else {
                    return
                }
                
                completion(strongSelf.currentTime)
            }
        )
        
        timer?.fire()
        
        return true
    }
    
    /// Stop tracking playback time.
    /// - Returns: True if tracking is already in progress, false otherwise.
    @available(iOS 10.0, *)
    @discardableResult
    public func removeTimeTracking() -> Bool {
        if timer == nil {
            return false
        }

        timer?.invalidate()
        timer = nil
        
        return true
    }
}

extension AudioPlayer {
    /// Perform callback for this player.
    func performCallback() {
        callback?(self)
    }
}

