//
//  AudioPlayerManagerDelegate.swift
//  Audio
//
//  Created by Endoral on 13.07.2018.
//  Copyright © 2018 Endoral. All rights reserved.
//

import Foundation

/// The delegate of the manager of audio players to track global audio events and events related to the playback status of players.
@objc
public protocol AudioPlayerManagerDelegate: AnyObject {    
    /// Performed after the player has finished playing.
    /// - Parameters:
    ///     -  player: Player ending playback.
    @objc
    optional func audioPlayerDidFinishPlaying(player: AudioPlayer)
    
    /// Performed after calling the stop player function.
    /// - Parameters:
    ///     - player: The player that was stopped.
    @objc
    optional func audioPlayerDidStop(player: AudioPlayer)
}

