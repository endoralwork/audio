//
//  AudioPlayerDelegate.swift
//  Audio
//
//  Created by Endoral on 12.07.2018.
//  Copyright © 2018 Endoral. All rights reserved.
//

import AVFoundation

/// Audio player delegate to track completion and stop of playback.
@objc
public protocol AudioPlayerDelegate: AnyObject, AVAudioPlayerDelegate {
    /// It is executed after calling the stop player function.
    /// - Parameters:
    ///     - player: The player that was stopped.
    @objc
    optional func audioPlayerDidStop(_ player: AudioPlayer)
}

