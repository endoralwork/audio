//
//  AudioPlayerManager.swift
//  Audio
//
//  Created by Endoral on 12.07.2018.
//  Copyright © 2018 Endoral. All rights reserved.
//

import AVFoundation

/// Manager for audio players. Allow to manage all player's properties from one point.
final class AudioPlayerManager: NSObject {
    /// Key to save current volume.
    private let currentVolumeKey: String
    
    /// Key to save maximum volume.
    private let maxVolumeKey: String
    
    /// Key to save mute state.
    private let isMutedKey: String
    
    /// Current active players.
    private(set) var players: Dictionary<AudioPlayer.ID, AudioPlayer> = [:]
    
    /// The value above which the volume of the players cannot be set (except for cases when the player volume is set directly). 1 by default.
    private(set) var maxVolume: Float {
        didSet {
            Prefs.setValue(maxVolume, for: maxVolumeKey)
        }
    }
    
    /// Last non-zero manager volume. No adjustment for mute state. Serves to restore the player volume after mute. Value of maxVolume (1) by default.
    private(set) var currentVolume: Float {
        didSet {
            Prefs.setValue(currentVolume, for: currentVolumeKey)
        }
    }
    
    /// Mute state. If true, then the volume value is set to 0, otherwise the volume value of the players is set to the currentVolume value. False by default.
    private(set) var isMuted: Bool {
        didSet {
            Prefs.setValue(isMuted, for: isMutedKey)
        }
    }
    
    /// Current player volume. Adjusted for mute state. If muted then 0, otherwise currentVolume.
    private(set) var volume: Float
    
    /// The delegate to track global audio events and events related to the playback status of players.
    weak var delegate: AudioPlayerManagerDelegate?
    
    /// Manager for audio players with specified keys to save global values.
    /// - Parameters:
    ///     - currentVolumeKey: Key to save current volume.
    ///     - maxVolumeKey: Key to save maximum volume.
    ///     - isMutedKey: Key to save mute state.
    init(currentVolumeKey: String, maxVolumeKey: String, isMutedKey: String) {
        self.currentVolumeKey = currentVolumeKey
        self.maxVolumeKey = maxVolumeKey
        self.isMutedKey = isMutedKey
        
        maxVolume = Prefs.getValue(for: maxVolumeKey, default: 1)
        currentVolume = Prefs.getValue(for: currentVolumeKey, default: maxVolume)
        isMuted = Prefs.getValue(for: isMutedKey, default: false)
        volume = isMuted
            ? 0
            : currentVolume
    }
}

extension AudioPlayerManager {
    /// Available audio formats.
    static let extensions = [
        "wav",
        "mp3",
        "m4a"
    ]
}
 
extension AudioPlayerManager {
    /// Add player to active list.
    /// - Parameters:
    ///     - player: Player to add to active list.
    ///     - volumePercent: Percentage of sound volume of the added player from 0 to 100, where 100 is equal to maxVolume.
    func add(player: AudioPlayer, volumePercent: Float) {
        player.setVolumePercent(volumePercent, maxVolume: volume)
        
        players[player.id] = player
    }
    
    /// Remove player from active list.
    /// - Parameters:
    ///     - player: Player to remove from the list of active.
    /// - Returns: A player that was removed or nil if such a player was not in the list of active.
    @discardableResult
    func remove(player: AudioPlayer) -> AudioPlayer? {
        return removePlayer(for: player.id)
    }
    
    /// Remove player from active list.
    /// - Parameters:
    ///     - id: Player ID to remove this player from the list of active.
    /// - Returns: A player that was removed or nil if such a player was not in the list of active.
    @discardableResult
    func removePlayer(for id: AudioPlayer.ID) -> AudioPlayer? {
        return players.removeValue(forKey: id)
    }
    
    /// Player for specified id.
    /// - Parameters:
    ///     - id: Player id.
    /// - Returns: The player associated with the specified id or nil if the player associated with the specified id is missing.
    func player(for id: AudioPlayer.ID) -> AudioPlayer? {
        return players[id]
    }
    
    /// Stop playback of all sounds.
    func stopAll() {
        players.values.forEach { player in
            player.stop()
        }
    }
    
    /// Set mute state for all players.
    /// - Parameters:
    ///     - isMuted: Mute state. If true then sets the player volume to 0, otherwise sets the player volume to currentVolume.
    ///     - fadeDuration: Volume fade duration.
    func set(isMuted state: Bool, fadeDuration: TimeInterval = 0.0) {
        let newVolume = state
            ? 0
            : currentVolume
        
        setVolume(newVolume, fadeDuration: fadeDuration)
        
        isMuted = state
    }
    
    /// Set the volume for all players.
    /// - Parameters:
    ///     - volume: Sound volume value from 0 to maxVolume.
    ///     - fadeDuration: Volume fade duration.
    /// - Returns: True if the specified value is set successfully, otherwise false.
    @discardableResult
    func set(volume value: Float, fadeDuration: TimeInterval = 0.0) -> Bool {
        switch value {
        case let x where x == 0:
            set(isMuted: true, fadeDuration: fadeDuration)
        
        case let x where x > 0 && x <= maxVolume:
            currentVolume = value
            
            if isMuted {
                set(isMuted: false, fadeDuration: fadeDuration)
            } else {
                setVolume(currentVolume, fadeDuration: fadeDuration)
            }
            
        default:
            return false
        }
        
        return true
    }
    
    /// Set the maximum volume value for all players.
    /// - Parameters:
    ///     - maxVolume: Maximum volume value from >0 to 1.
    /// - Returns: True if the specified value is set successfully, otherwise false.
    @discardableResult
    func set(maxVolume value: Float) -> Bool {
        switch value {
        case let x where x > 0 && x <= 1:
            maxVolume = value
            
        default:
            return false
        }
        
        return true
    }
    
    /// Set the volume at the first launch.
    /// - Parameters:
    ///     - defaultVolume: Volume value from 0 to maxVolume.
    /// - Returns: True if the specified value is set successfully, otherwise false.
    @discardableResult
    func set(defaultVolume value: Float) -> Bool {
        if value == 0 && Prefs.isExist(valueFor: isMutedKey) {
            Prefs.setValue(currentVolume, for: currentVolumeKey)
            
            return false
        }
        
        if Prefs.isExist(valueFor: currentVolumeKey) {
            return false
        }
        
        Prefs.setValue(currentVolume, for: currentVolumeKey)
        
        return set(volume: value)
    }
    
    /// Reset the flag responsible for locking the ability to set the default volume value.
    func resetDefaultVolumeState() {
        Prefs.removeValue(for: currentVolumeKey)
        Prefs.synchronize()
    }
}

extension AudioPlayerManager {
    /// Set volume for all active players
    /// - Parameters:
    ///     - value: New volume value.
    ///     - fadeDuration: Volume fade duration.
    private func setVolume(_ value: Float, fadeDuration: TimeInterval = 0.0) {
        volume = value
        
        players.values.forEach { player in
            if #available(iOS 10.0, *) {
                player.setVolume(value, fadeDuration: fadeDuration)
            } else {
                player.volume = value
            }
        }
    }
}

extension AudioPlayerManager: AudioPlayerDelegate {
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        guard let audioPlayer = player as? AudioPlayer else {
            return
        }
        
        audioPlayer.performCallback()
        
        delegate?.audioPlayerDidFinishPlaying?(player: audioPlayer)
    }
    
    func audioPlayerDidStop(_ player: AudioPlayer) {
        removePlayer(for: player.id)
        
        delegate?.audioPlayerDidStop?(player: player)
    }
}

