//
//  AudioTests.swift
//  AudioTests
//
//  Created by Endoral on 27.07.2018.
//  Copyright © 2018 Endoral. All rights reserved.
//

import XCTest
import Audio

class AudioTests: XCTestCase {
    private let bundle = Bundle(for: AudioTests.self)
    
    private var softClickUrl: URL!
    
    private var hardClickUrl: URL!
    
    private var doubleClickUrl: URL!
    
    private var longMultipleClickUrl: URL!
    
    override func setUp() {
        super.setUp()
        
        if let softClickUrl = bundle.url(forResource: "softClick", withExtension: "mp3") {
            self.softClickUrl = softClickUrl
        }

        if let hardClickUrl = bundle.url(forResource: "hardClick", withExtension: "mp3") {
            self.hardClickUrl = hardClickUrl
        }

        if let doubleClickUrl = bundle.url(forResource: "doubleClick", withExtension: "mp3") {
            self.doubleClickUrl = doubleClickUrl
        }

        if let longMultipleClickUrl = bundle.url(forResource: "longMultipleClick", withExtension: "mp3") {
            self.longMultipleClickUrl = longMultipleClickUrl
        }
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        
        super.tearDown()
    }
    
    func test1SoundsIsNotNil() {
        XCTAssertNotNil(softClickUrl)
        XCTAssertNotNil(hardClickUrl)
        XCTAssertNotNil(doubleClickUrl)
        XCTAssertNotNil(longMultipleClickUrl)
    }
    
    func test2UpperVolumeLimitPositive() {
        let upValue: Float = 1
        
        XCTAssertTrue(Sound.set(maxVolume: upValue))
        XCTAssertEqual(Sound.maxVolume, upValue)
        
        let downValue: Float = 0.01
        
        XCTAssertTrue(Sound.set(maxVolume: downValue))
        XCTAssertEqual(Sound.maxVolume, downValue)
    }
    
    func test3UpperVolumeLimitNegative() {
        let upValue: Float = 1.1
        
        XCTAssertFalse(Sound.set(maxVolume: upValue))
        XCTAssertNotEqual(Sound.maxVolume, upValue)
        
        let downValue: Float = 0
        
        XCTAssertFalse(Sound.set(maxVolume: downValue))
        XCTAssertNotEqual(Sound.maxVolume, downValue)
    }
    
    func test4DefaultVolumeSet() {
        Sound.set(maxVolume: 1)
        Sound.resetDefaultVolumeState()
        
        //  Set currentVolume to 0.5
        let value: Float = 0.5
        
        XCTAssertTrue(Sound.set(defaultVolume: value))
        XCTAssertEqual(Sound.volume, value)
    }
    
    func test5SoundsMuted() {
        Sound.set(isMuted: true)
        
        XCTAssertTrue(Sound.isMuted)
        XCTAssertEqual(Sound.volume, 0)
    }
    
    func test6CurrentVolume() {
        // 0.5 is value from test4DefaultVolumeSet
        let value: Float = 0.5
        
        XCTAssertEqual(Sound.currentVolume, value)
    }
    
    func test7SoundsVolumeSetToValue() {
        let value: Float = 0.2
        
        XCTAssertTrue(Sound.set(volume: value))
        XCTAssertFalse(Sound.isMuted)
        XCTAssertEqual(Sound.volume, value)
    }
    
    func test8SoundsVolumeSetToZeroGoesMuted() {
        XCTAssertTrue(Sound.set(volume: 0))
        XCTAssertTrue(Sound.isMuted)
    }
    
    func test9SoundsNotMuted() {
        Sound.set(isMuted: false)
        
        XCTAssertFalse(Sound.isMuted)
        XCTAssertNotEqual(Sound.volume, 0)
    }
    
    func test10PlayerCreate() {
        let expectation = XCTestExpectation()
        
        expectation.assertForOverFulfill = true
        
        let playerFromName1 = Sound.play(name: "doubleClick.mp3", bundle: bundle, id: 1) { player in
            //  Must not be executed
            XCTAssertTrue(false)
        }
        
        let playerFromName2 = Sound.play(name: "doubleClick.mp3", bundle: bundle, id: 1)
        
        let playerFromUrl = Sound.play(url: doubleClickUrl) { player in
            //  Must be executed only once
            expectation.fulfill()
        }

        XCTAssertNotNil(playerFromName1)
        XCTAssertNotNil(playerFromName2)
        XCTAssertNotNil(playerFromUrl)
        XCTAssertEqual(Sound.players.count, 2)
        
        if let player1 = playerFromName1 {
            player1.stop()
            
            XCTAssertFalse(Sound.players.contains(player1))
        }

        if let player = playerFromUrl {
            wait(for: [expectation], timeout: player.duration + 0.5)
        }
    }
    
    func test11BackgroundMusic() {
        BackgroundMusic.set(volume: 1)
        
        let expectation = XCTestExpectation()
        
        guard let player = BackgroundMusic.play(name: "longMultipleClick.mp3", bundle: bundle) else {
            // if player is nil then return
            XCTAssert(false)
            
            return
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + player.duration) {
            expectation.fulfill()
        }
        
//        BackgroundMusic.set(isMuted: true, fadeDuration: 5)
        
        wait(for: [expectation], timeout: player.duration + 0.5)
    }
    
//    func testPerformanceExample() {
//        // This is an example of a performance test case.
//        self.measure {
//            // Put the code you want to measure the time of here.
//        }
//    }
}









