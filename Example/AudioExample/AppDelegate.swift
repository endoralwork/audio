//
//  AppDelegate.swift
//  AudioExample
//
//  Created by Endoral on 13.07.2018.
//  Copyright © 2018 Endoral. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        #if DEBUG
            print("\(classForCoder): application(_:, didFinishLaunchingWithOptions:)")
        #endif
        
        return true
    }

    func applicationWillTerminate(_ application: UIApplication) {
        #if DEBUG
            print("\(classForCoder): applicationWillTerminate(_:)")
        #endif
    }
}









