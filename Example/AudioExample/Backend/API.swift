//
//  API.swift
//  AudioExample
//
//  Created by Endoral on 27.07.2018.
//  Copyright © 2018 Endoral. All rights reserved.
//

import Foundation
import Audio

final class API {
    enum ActionType: Int {
        case playWithID = 1
        
        case play = 2
        
        case stopAll = 3
    }
    
    static let soundName = "longMultipleClick"
    
    static func action(
        type actionType: ActionType,
        id: String? = nil
    ) {
        switch actionType {
        case .playWithID:
            let soundID = id ?? "Sound_\(actionType.rawValue)"
            
            play(id: soundID)
            
        case .play:
            play()
            
        case .stopAll:
            stopAll()
        }
    }
    
    private static func play(id: String) {
        Sound.play(name: soundName, id: id) { player in
            print("\(player.id): \(player) has finished playback")
        }
    }
    
    private static func play() {
        Sound.play(name: soundName) { player in
            print("\(player.id): \(player) has finished playback")
        }
    }
    
    private static func stopAll() {
        Sound.stopAll()
    }
    
    private init() {
        
    }
}









