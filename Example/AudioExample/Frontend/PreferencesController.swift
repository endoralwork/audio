//
//  PreferencesController.swift
//  AudioExample
//
//  Created by Endoral on 13.07.2018.
//  Copyright © 2018 Endoral. All rights reserved.
//

import UIKit
import Audio

class PreferencesController: UIViewController {
    @IBOutlet weak var slider: UISlider! {
        didSet {
            slider?.maximumValue = Sound.maxVolume
            slider?.value = Sound.volume
        }
    }
    
    @IBOutlet weak var minButton: UIButton!
    
    @IBOutlet weak var maxButton: UIButton!
    
    @IBOutlet weak var muteButton: UIButton! {
        didSet {
            muteButton?.isSelected = Sound.isMuted
        }
    }
    
    deinit {
        print("\(classForCoder): deinit")
    }
    
    @IBAction func slide(_ sender: UISlider) {
        Sound.set(volume: sender.value)
        
        muteButton?.isSelected = Sound.isMuted
        minButton?.isSelected = Sound.isMuted
        maxButton?.isSelected = !Sound.isMuted && Sound.volume == Sound.maxVolume
    }
    
    @IBAction func minClick(_ sender: UIButton) {
        let newValue: Float = 0
        
        Sound.set(volume: newValue)
        
        slider?.setValue(newValue, animated: false)
        muteButton?.isSelected = Sound.isMuted
        sender.isSelected = Sound.isMuted
        maxButton?.isSelected = !Sound.isMuted && Sound.volume == Sound.maxVolume
    }
    
    @IBAction func maxClick(_ sender: UIButton) {
        let newValue: Float = Sound.maxVolume
        
        Sound.set(volume: newValue)
        
        slider?.setValue(newValue, animated: false)
        muteButton?.isSelected = Sound.isMuted
        minButton?.isSelected = Sound.isMuted
        sender.isSelected = !Sound.isMuted && Sound.volume == Sound.maxVolume
    }
    
    @IBAction func muteClick(_ sender: UIButton) {
        let newState = !Sound.isMuted
        
        Sound.set(isMuted: newState)
        
        slider?.setValue(Sound.volume, animated: false)
        sender.isSelected = Sound.isMuted
        minButton?.isSelected = Sound.isMuted
        maxButton?.isSelected = !Sound.isMuted && Sound.volume == Sound.maxVolume
    }
    
    @IBAction func soundsClick(sender: UIButton) {
        guard let actionType = API.ActionType(rawValue: sender.tag) else {
            return print("\(classForCoder): couldn't get action type from \(sender.tag)")
        }
        
        let soundID = "\(sender.hashValue)"
        
        API.action(type: actionType, id: soundID)
    }
}









