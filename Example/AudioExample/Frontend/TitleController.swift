//
//  ViewController.swift
//  AudioExample
//
//  Created by Endoral on 13.07.2018.
//  Copyright © 2018 Endoral. All rights reserved.
//

import UIKit

class TitleController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("\(classForCoder): viewDidLoad()")
    }
    
    @IBAction func soundsClick(sender: UIButton) {
        guard let actionType = API.ActionType(rawValue: sender.tag) else {
            return print("\(classForCoder): couldn't get action type from \(sender.tag)")
        }
        
        let soundID = "\(sender.hashValue)"
        
        API.action(type: actionType, id: soundID)
    }
}









