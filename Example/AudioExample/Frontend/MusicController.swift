//
//  MusicController.swift
//  AudioExample
//
//  Created by Endoral on 18/02/2019.
//  Copyright © 2019 Endoral. All rights reserved.
//

import UIKit
import Audio

class MusicController: UIViewController {
    @IBOutlet private weak var playButton: UIButton!
    
    @IBOutlet private weak var pauseButton: UIButton!
    
    @IBOutlet private weak var stopButton: UIButton!
    
    @IBOutlet private weak var releaseButton: UIButton!
    
    @IBOutlet private weak var muteButton: UIButton! {
        didSet {
            muteButton?.isSelected = BackgroundMusic.isMuted
        }
    }
    
    @IBOutlet private weak var minButton: UIButton! {
        didSet {
            minButton?.isSelected = BackgroundMusic.isMuted
        }
    }
    
    @IBOutlet private weak var maxButton: UIButton! {
        didSet {
            maxButton?.isSelected = !BackgroundMusic.isMuted && BackgroundMusic.volume == BackgroundMusic.maxVolume
        }
    }
    
    @IBOutlet private weak var slider: UISlider! {
        didSet {
            slider?.maximumValue = BackgroundMusic.maxVolume
            slider?.setValue(BackgroundMusic.volume, animated: false)
        }
    }
}

extension MusicController {
    @IBAction private func playClick(_ sender: UIButton) {
        BackgroundMusic.play()
    }
    
    @IBAction private func pauseClick(_ sender: UIButton) {
        BackgroundMusic.pause()
    }
    
    @IBAction private func stopClick(_ sender: UIButton) {
        BackgroundMusic.stop()
    }
    
    @IBAction private func releaseClick(_ sender: UIButton) {
        BackgroundMusic.release()
    }
    
    @IBAction private func muteClick(_ sender: UIButton) {
        let newState = !BackgroundMusic.isMuted
        
        BackgroundMusic.set(isMuted: newState)
        
        slider?.setValue(BackgroundMusic.volume, animated: false)
        sender.isSelected = BackgroundMusic.isMuted
        minButton?.isSelected = BackgroundMusic.isMuted
        maxButton?.isSelected = !BackgroundMusic.isMuted && BackgroundMusic.volume == BackgroundMusic.maxVolume
    }
    
    @IBAction private func minClick(_ sender: UIButton) {
        let newValue: Float = 0
        
        BackgroundMusic.set(volume: newValue)
        
        slider?.setValue(newValue, animated: false)
        muteButton?.isSelected = BackgroundMusic.isMuted
        sender.isSelected = BackgroundMusic.isMuted
        maxButton?.isSelected = !BackgroundMusic.isMuted && BackgroundMusic.volume == BackgroundMusic.maxVolume
    }
    
    @IBAction private func maxClick(_ sender: UIButton) {
        let newValue: Float = BackgroundMusic.maxVolume
        
        BackgroundMusic.set(volume: newValue)
        
        slider?.setValue(newValue, animated: false)
        muteButton?.isSelected = BackgroundMusic.isMuted
        minButton?.isSelected = BackgroundMusic.isMuted
        sender.isSelected = !BackgroundMusic.isMuted && BackgroundMusic.volume == BackgroundMusic.maxVolume
    }
    
    @IBAction private func slide(_ sender: UISlider) {
        BackgroundMusic.set(volume: sender.value)
        
        muteButton?.isSelected = BackgroundMusic.isMuted
        minButton?.isSelected = BackgroundMusic.isMuted
        maxButton?.isSelected = !BackgroundMusic.isMuted && BackgroundMusic.volume == BackgroundMusic.maxVolume
    }
}









